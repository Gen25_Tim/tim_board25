declare module "@salesforce/apex/Ctrl_Board25.getRecords" {
  export default function getRecords(param: {SObjectType: any, StageValueField: any, fieldNames: any}): Promise<any>;
}
declare module "@salesforce/apex/Ctrl_Board25.saveRecord" {
  export default function saveRecord(param: {SObjectType: any, StageValueField: any, value: any, recordId: any}): Promise<any>;
}
