public class Ctrl_Board25 {
	//public List<Schema.PicklistEntry> statuses  { get; set; }

	@AuraEnabled
	public static List<Util_StageWrapper> getRecords(String SObjectType, String StageValueField, String fieldNames){

		Map<String, Schema.SObjectType> globalMap = Schema.getGlobalDescribe();
		Schema.DescribeSObjectResult obj = globalMap.get(SObjectType).getDescribe();
		Map<String, Schema.SObjectField> fieldMap = obj.fields.getMap();

		Schema.DescribeFieldResult configField = fieldMap.get(StageValueField).getDescribe();
		List<Schema.PicklistEntry> ples = configField.getPicklistValues();

		Map<String, Util_StageWrapper> statusesByName = new Map<String, Util_StageWrapper>();
		List<Util_StageWrapper> stgWrapperList = new List<Util_StageWrapper>();
		for (Schema.PicklistEntry ple : ples){
			String statusName = ple.GetLabel();

			Util_StageWrapper stgWrapper = new Util_StageWrapper();
			stgWrapper.stageName = statusName;

			statusesByName.put(statusName, stgWrapper);
			stgWrapperList.add(stgWrapper);
		}

		List<String> fieldNamesList = fieldNames.split(',');

		String queryStr='SELECT Id, ' + String.escapeSingleQuotes(StageValueField) + ', ' +
			String.escapeSingleQuotes(fieldNames) + 
			' FROM ' + String.escapeSingleQuotes(SObjectType);
		List<SObject> sobjects=Database.query(queryStr);
		
		for (SObject sobj : sobjects){
			String value = String.valueOf(sobj.get(StageValueField));
			Util_StageWrapper stg = statusesByName.get(value);
			if(stg != null){
				Util_StageWrapper.StageSObject sso = new Util_StageWrapper.StageSObject();

				sso.titleField = new Util_StageWrapper.StageSObjectField('Id', 'Id', sobj.Id);

				for (String fieldName : fieldNamesList){
					fieldName = fieldName.trim();
					Schema.DescribeFieldResult fieldRes = fieldMap.get(fieldName).getDescribe();
                    Util_StageWrapper.StageSObjectField ssoField =  new Util_StageWrapper.StageSObjectField(fieldRes.getLabel(), fieldName, sobj.get(fieldName));
					sso.fields.add(ssoField);                    
				}
				stg.sobjects.add(sso);
			}
		}
		return statusesByName.values();
	}

	@AuraEnabled
	public static void saveRecord(String SObjectType, String StageValueField, String value, String recordId){
		Schema.SObjectType convertType = Schema.getGlobalDescribe().get(SObjectType);
		SObject genericObject = convertType.newSObject();

		genericObject.Id = recordId;
		genericObject.put(StageValueField, value);
		genericObject.put('Id', recordId);
		Database.update(genericObject); 
	}
}