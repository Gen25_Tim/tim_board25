public with sharing class Util_StageWrapper {
    @AuraEnabled public String stageName {get; set;}
    @AuraEnabled public List<StageSObject> sobjects {get; set;}

    public Util_StageWrapper() {
        sobjects=new List<StageSObject>();
    }
    
    public class StageSObject{
        @AuraEnabled public StageSObjectField titleField {get; set;}
        @AuraEnabled public List<StageSObjectField> fields {get; set;}
        
        public StageSObject() {
            fields=new List<StageSObjectField>();
        }
    }

    public class StageSObjectField {
        @AuraEnabled public String fieldName {get; set;}
        @AuraEnabled public String fieldApiName {get; set;}
        @AuraEnabled public String fieldValue {get; set;}
        
        public StageSObjectField(String fName, String fApiName, Object fValue) {
            fieldName=fName;
            fieldApiName=fApiName;
            fieldValue=String.valueOf(fValue);
        }
    }
}
