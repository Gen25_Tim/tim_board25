({
	allowDrop: function(component, event, helper) {
        event.preventDefault();        
    },
    
    onDrop: function(component, event, helper) {
        event.preventDefault();
        var columnChangeEvent = component.getEvent('columnChange');
        columnChangeEvent.setParams({
            'title': component.get('v.title'),
            'item': JSON.parse(event.dataTransfer.getData('text'))
        });
        columnChangeEvent.fire();
    },
})