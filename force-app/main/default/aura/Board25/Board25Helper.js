({
	loadRecords : function(component, event) {
		var action = component.get("c.getRecords");
        action.setParams({
            "SObjectType" : component.get("v.SObjectType"),
            "StageValueField" : component.get("v.StageValueField"),
            "fieldNames" : component.get("v.Fields")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log(response.getReturnValue());
                component.set('v.allRecords', response.getReturnValue());
            }

        });

        $A.enqueueAction(action);
	},
	saveRecord : function(component, event, record, value){
		var action = component.get("c.saveRecord");
		console.log(component.get('v.allRecords'));
        /*action.setParams({
            "SObjectType" : component.get("v.SObjectType"),
            "StageValueField" : component.get("v.StageValueField"),
			"value" : value,
			"recordId" : record.sobjects[0].titleField.fieldValue
		});
		
		action.setCallback(this, function (response) {
			var state = response.getState();
            if (state === 'SUCCESS') {
				this.loadRecords(component, event);
            }

		});

		$A.enqueueAction(action);*/
	}
})