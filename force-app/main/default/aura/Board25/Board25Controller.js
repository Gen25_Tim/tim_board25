({
	doInit : function(component, event, helper) {
        helper.loadRecords(component, event);
	},
    
    onColumnChange: function(component, event, helper) {
        var title = event.getParam('title');
        var item = event.getParam('item');
        helper.saveRecord(component, event, item, title);

        /*
        var allLists = component.get('v.allRecords');
        console.log(allLists);
        var actualItem = allLists.find(function(el) {
            console.log(el);
            return el.id == item.id;
        });
        
        if (actualItem) {
            // actualItem.status = title;
            // console.log(actualItem);
            // component.set('v.allRecords', allLists);
            // alert('item');
        } else {
            console.log('could not find item ', item, ' in list ', allLists);
        }
        */
        
    }
 })